import 'dart:async';

void main() {
  print("Do i wanna know - Arctic Monkeys");
  print("");
  var timer1 = Timer(Duration(seconds: 2), () => print("Crawling back to you"));
  var timer2 = Timer(Duration(seconds: 5), () => print("Ever thought of calling when"));
  var timer3 = Timer(Duration(seconds: 8), () => print("you've had a few?"));
  var timer4 = Timer(Duration(seconds: 11), () => print("'Cause I always do"));
  var timer5 =
      Timer(Duration(seconds: 15), () => print("Maybe I'm too"));
  var timer6 = Timer(Duration(seconds: 18), () => print("busy being yours to fall for somebody new"));
  var timer7 =
      Timer(Duration(seconds: 21), () => print("Now I've thought it through"));
  var timer8 = Timer(Duration(seconds: 24), () => print("Crawling back to you"));
  var timer9 = Timer(Duration(seconds: 25), () => print(""));

  var timer10 = Timer(Duration(seconds: 29),
      () => print(""));
}
